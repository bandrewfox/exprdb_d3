// var subset = subObject(elmo, ["color", "height"]);
// https://stackoverflow.com/questions/17781472/how-to-get-a-subset-of-a-javascript-objects-properties
function subObject(sourceObject, keys) {
  var newObject = {};
  keys.forEach(function (key) {
    newObject[key] = sourceObject[key];
  });
  return newObject;
}

function combineObjects(firstObject, secondObject) {
  var newObject = {};
  Object.keys(firstObject).forEach(function (key) {
    newObject[key] = firstObject[key];
  });
  Object.keys(secondObject).forEach(function (key) {
    newObject[key] = secondObject[key];
  });
  return newObject;
}

function objectValues(inputObject) {
  var newArray = [];
  Object.keys(inputObject).forEach(function (key) {
    newArray[newArray.length] = inputObject[key];
  });
  return newArray;
}

function redrawFields(schemaToRedraw, parentNodeId, state) {
  // set the default values
  // schemaToRedraw = (typeof schemaToRedraw !== "undefined") ? schemaToRedraw : fields_schema;
  // parentNodeId = (typeof parentNodeId !== "undefined") ? parentNodeId : "#inputs";
  // start by removing all <div> with matching ids in the schemaToRedraw which are children of the #inputs <div>
  Object.keys(schemaToRedraw).forEach(function (k) {
    d3.select(parentNodeId)
      .select("#parent-" + schemaToRedraw[k].id)
      .remove();
  });

  // then insert everything into the div
  d3.select(parentNodeId)
    .selectAll("div")
    .data(objectValues(schemaToRedraw)) // I could use Object.values(), but it isn't supported in IE
    .enter()
    .append("div")
    .attr("class", "form-group")
    .each(function (d) {
      var self = d3.select(this);
      self.attr("id", "parent-" + d.id);

      if (d.display !== null && d.display !== "") {
        self.append("label").attr("for", d.id).text(d.display);
      }

      var field_class = "form-control";
      if (d.element_class) {
        field_class = d.element_class;
      }

      if (d.type === "text") {
        var new_input_field = self
          .append("input")
          .attr("class", field_class)
          .attr({
            type: function (d) {
              return d.type;
            },
            id: function (d) {
              return d.id;
            },
            name: function (d) {
              return d.name;
            },
          });
        if (d.value) {
          new_input_field.attr("value", d.value);
        }
      }

      if (d.type === "select2") {
        var new_input_field = self
          .append("select")
          .attr("class", field_class)
          .attr({
            id: function (d) {
              return d.id;
            },
            name: function (d) {
              return d.name;
            },
          });

        $(document).ready(function () {
          let placeholder = "";
          if (state.gene != null) {
            console.log("state", state);
            console.log("state.gene", state.gene);
            placeholder = state.gene;
          }
          $("#" + d.id).select2({
            placeholder: placeholder,
            allowClear: true,
            minimumInputLength: 2,
            data: [],
            ajax: {
              url: function (params) {
                return (
                  "http://demo.needlegenomics.com/feature_api/feature/?search_type=begins&organism=" +
                  state.organism +
                  "&name=" +
                  params.term
                );
              },
              dataType: "json",
              type: "GET",
              delay: 250,
              processResults: function (data) {
                let resultsArr = [];

                data.results.forEach(function (d, i) {
                  state.index += i;
                  let tmpObj = { id: state.index, text: d["name"] };
                  resultsArr.push(tmpObj);
                });
                return {
                  results: resultsArr,
                };
              },
            },
          });
        });
      }

      if (d.type === "dropdown" && d.values !== null) {
        var new_select_field = self
          .append("select")
          .attr("class", field_class)
          .attr({
            type: function (d) {
              return d.type;
            },
            id: function (d) {
              return d.id;
            },
            name: function (d) {
              return d.name;
            },
          })
          .selectAll("option")
          .data(d.values)
          .enter()
          .append("option")
          .attr("value", function (d) {
            if (d.length === 2) {
              // if d is an array of length 2 then the first element is the option value
              // and the second element is the displayed text
              return d[0];
            } else {
              return d;
            }
          })
          .text(function (d) {
            if (d.length === 2) {
              return d[1];
            } else {
              return d;
            }
          });
        if (d.selected) {
          new_select_field.property("selected", function (d2) {
            if (d2.length === 2) {
              return d2[0] === d.selected;
            } else {
              return d2 === d.selected;
            }
          });
        }
      }
    });
}
