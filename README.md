# exprdb d3 plots

## Overview

The js code in this repo is for the d3 based plotting from the exprdb app. The main intention is
to allow contractors to work with this code base without needing the rest of the app code. In order
to use the app with real data on a live server, the demo server for the exprdb app is running and
the address is in the code.

## How to run

In order to run this js code, you need to copy it to a directory on your computer and open 
the index_xxx.html page of interest. However, due to cross origin requests (CORS) via js being blocked by 
all browsers, it is required to install a Chrome extension in order to allow CORS and also to
add the Authentication token to the HTTP request header so that the demo site will return data.

### Configuration of ModHeader by bewisse.com

Before loading the index_xxx.html page into your Chrome browser, you need to install an extension 
to allow CORS and add a Auth header: [ModHeader](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj)

You need to add a Request header with the Name of "Authorization" and the Value of "Token abcdefgh123456789" but don't
use abcdefg...., you will instead use a token provided by someone who has access to the 
[demo site](http://demo.needlegenomics.com).

You also need to add a Response Header with the "+" sign at the top of the configuration window. The Name for this one
is "Access-Control-Allow-Origin" and the Value is "*" (no quotes). 

The reviews seemed good for this extension, but you probably still want to pause or disable it when not doing
active development, or you can also "lock tab" so that it only is modifying headers on the exprdb index html tab.

### How to use the plot app

You can view different datasets (called "series") and also type in genes of interest. After selecting a new Series,
the "Group By" and "Color By" dropdowns choices get filled in depending on the Series you selected.  Every Series 
has any number of samples, typically ranging from 20 to several thousand. Each Sample within each Series has about
20,000 genes which have expression values.  Each Sample also has a collection of 5-20 different annotations (meta-data)
which is used to determine the group and color for each Gene in the Dataset.

Here are some example genes (you need to match the case provided - and for human data, usually genes are all CAPS):

* IL2
* CD8A
* FAP
* TNF
* IL6
* CDK2

When you enter ina new gene, you can hit your tab or enter key in order to do the ajax (d3.csv) call to get the 
new data.

### API calls

There are 3 main API calls:

* getting a list of all the different datasets
* getting the "group by/color by" information for a particular dataset (the vector of group and color values are the same)
* getting sample annotations and expression values of all samples for a gene in a particular dataset (like I said 
before, some Series have a handful of samples and other Series could have 1000s of samples. Each sample has 2-20 
different annotations which are used in order to figure out the group or color for that sample)

### Example data for development usage

I used the dev tools in Chrome to save the Response data into separate files and put them in  the fixtures directory in this repository 
for the 3 main API calls in the bfoxplot.js workflow:

* _series_api.series.json_: these data are downloaded via the API when the page is first rendered. It has all the 
dataset/series names, along with the organism and series_id so that the "Series" dropdown can be populated with
the available choices to the logged in user.
* _series_api.series_data_view.samples_labels.csv_: once a series is choosen, then this csv file is retrieved and it
has all the meta-data information for all the samples, including the sample ids, sample names, various columns
which can be used for grouping and coloring, and potentially the tSNE or PCA coordinates. After this file is loaded,
then the column names of the csv file are added as choices to the Group by and Color by drop downs. The meta-data
for each sample is also saved so that the app can use this to determine the group and color for each point.
* _series_api.series_data_view.samples_labels.TNF.csv_: Once a gene is typed into the Gene box (with the proper case), 
then this csv file is retrieved from the server. This has the expression value for the one gene of interest in every 
sample of the series/dataset. (It also has all the sample meta-data information, which seems redundant and I can
probably exclude all that extra data from being needed for this 3rd call). I also have a couple other genes in 
the directory available.

