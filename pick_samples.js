

function getInputValue(elemSelector) {
    var elem = $(elemSelector);
    if (elem.length > 0) {
        return(elem[0].value);
    } else {
        return(null);
    }
}

// use save options as globals
var opts = {empty:1};

function getDatafilePath() {
    // 'http://127.0.0.1:8000/series_api/series_data_view/3?sample_labels=1&transpose=1&no_feat_info=1&name='
    url_params = "?sample_labels=1&transpose=1&no_feat_info=1";
    if (opts.geneset_pk === "") {
        url_params += "&name=" + opts.gene;
    } else {
        url_params += "&calc_feature_groups=" + opts.geneset_pk;
    }
    if (opts.keep_col_vals_url !== "") {
        url_params += "&" + opts.keep_col_vals_url;
    }
    return opts.series_data_url + opts.series_pk + url_params;
}

function processExprAndSubmitSamples(dat) {
    // the gene expr data is stored in an array using the gene name or the geneset_name
    // as the key. So, make a variable with that key
    var data_key = "";
    if (opts.geneset_pk === "") {
        data_key = opts.gene;
    } else {
        data_key = opts.geneset_name;
    }

    // dat contains the expression values for this gene so that we can figure out which samples
    if (dat.length > 0 && dat[0][data_key]) {
        // change the values in the gene expression column to numeric
        dat.forEach(function (d) {
            d[data_key] = +d[data_key];
        });
    } else {
        // if no data was found, then we can't set annots
        alert("No data found using that gene/geneset" + data_key);
        return;
    }

    if (! opts.name) {
        alert("Need to specify a label for the samples");
        return;
    }

    // this is important to prevent html injection
    if (opts.name.match(/[^A-Za-z0-9_\- ]/)) {
        alert("Label can only have plain characters, no symbols");
        return;
    }

    // built the object which will be submitted to the app to save the sample labels
    var annot_object = {
        general_type: "sample_annot",
        name: "sample annot: id=" + opts.series_pk + " owner:" + opts.owner,
        series_list: [opts.series_pk],
        owner_id: opts.owner,
        // csrfmiddlewaretoken: opts.csrf_token,
        details: null
    };

    if (opts.automatic_group_name) {
        if (opts.geneset_pk === "") {
            opts.cur_group = "gene:" + opts.gene;
        } else {
            opts.cur_group = "geneset:" + opts.geneset_name;
        }
    } else {
        // Get the selected group name or use the new group name if group == _new_group
        opts.cur_group = opts.group;
        var new_group_placeholder = "_new_group";
        if (opts.cur_group === new_group_placeholder) {
            if (opts.group_new === "") {
                alert("If you want a new group, then you need to fill in a new group name");
                return;
            }
            if (opts.group_new === new_group_placeholder) {
                alert("You can't use that name for a new group: " + new_group_placeholder);
                return;
            }
            // check if group_new has any invalid characters
            // (this is important to prevent html injection
            if (opts.group_new.match(/[^A-Za-z0-9_\- ]/)) {
                alert("New group can only have plain characters, no symbols");
                return;
            }
            opts.cur_group = opts.group_new;
        }
    }

    // Setup the details object which has group names as the keys and an additional object as each value.
    // Each additional object is an array with sample_id and name
    var newDetails = {};
    newDetails[opts.cur_group] = {};
    dat.forEach(function(d) {
        var keep = 0;
        if (opts.direction === "gt") {
            if (d[data_key] >= opts.threshold) {
                keep = 1;
            }
        } else if (opts.direction === "lt") {
            if (d[data_key] <= opts.threshold) {
                keep = 1;
            }
        }
        if (keep) {
            newDetails[opts.cur_group][d["_id"]] = opts.name;
        }
    });

    annot_object.details = newDetails;

    // Submit the info for the sample labels
    var ret = $.ajax({
        url: opts.modify_user_content_url,
        method: "POST",
        headers: {
            'Auth': 'Token ' + opts.auth_token,
            'Content-Type': 'application/json',
            // 'Cookie': {'csrftoken': null, 'sessionid': null}, // this doesnt work to set cookies via ajax
            'X-CSRFToken': opts.csrf_token,
            'Accept': 'application/json'
        },
        // xhrFields: {"withCredentials": true},
        success: function(x) {
            $(opts.output_elem_id)[0].innerHTML = "saved";
            refreshGroupNames(opts);
        },
        error: function(xhr, err1, err2) {
            $(opts.output_elem_id)[0].innerHTML = "Error: [" + xhr.status + " " + err2 + "]: " + xhr.responseText;
        },
        data: JSON.stringify(annot_object)
    });

}

// This function is called when the submit button is clicked
function submitSampleAnnots(cur_opts) {
    opts = cur_opts;
    d3.csv(getDatafilePath(), processExprAndSubmitSamples);
}

// this will check the database to get all the available group names
function refreshGroupNames(group_opts) {
    $.ajax({
        url: group_opts.modify_user_content_url,
        method: "GET",
        headers: {
            'Auth': 'Token ' + group_opts.auth_token,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        success: function(x) {
            // $(group_opts.output_elem_id)[0].innerHTML = "loading names";
            dat = x.results;
            // alert(dat[0].name);
            if (dat.length === 1) {
                var group_choices = [["_new_group", "use new group"]];
                Object.keys(dat[0].details).forEach(function (t) {
                    group_choices.push([t,t]);
                });
                if (! group_opts.automatic_group_name) {
                    group_opts.fields_schema.group.values = group_choices;
                    if (opts.cur_group) {
                        group_opts.fields_schema.group.selected = opts.cur_group;
                    }
                }
                redrawFields(group_opts.fields_schema, group_opts.annot_form_div);
                // $(group_opts.output_elem_id)[0].innerHTML = "ready";
                addFieldListeners(group_opts.fields_schema, group_opts.output_elem_id);
            }
        },
        error: function(xhr, err1, err2) {
            $(group_opts.output_elem_id)[0].innerHTML = "Error: [" + xhr.status + " " + err2 + "]: " + xhr.responseText;
        },
        data: {
            'series_id': group_opts.series_pk,
            'type': 'sample_annot',
            'mine': 1
        }

    })
}

// This will change the output_elem_id to blank any time a field is changed
function addFieldListeners(schema, output_elem_id) {
    Object.keys(schema).forEach(function(field) {
        var cur_selector = document.querySelector("#" + schema[field].id);
        if (cur_selector !== null) {
            cur_selector.addEventListener('change', function(x) {
                $(output_elem_id)[0].innerHTML = "";
            })
        }
    });
}

function pickByThreshold(cur_opts) {
    var annot_fields = {
        group: {
            name: 'group', id: 'sample_annot_group',
            element_class: 'sample_annot',
            type: 'dropdown', display: 'Main group name to use',
            values: [['_new_group', '(new group)']]
        },
        group_new: {
            name: 'group_new', element_class: 'sample_annot',
            id: 'sample_annot_group_new', type: 'text', display: 'Main group name (new one)', value: null
        },
        label_name: {
            name: 'name', element_class: 'sample_annot',
            id: 'sample_annot_name', type: 'text', display: 'Label for samples', value: null
        },
        threshold: {
            name: 'threshold', element_class: 'sample_annot',
            id: 'sample_annot_threshold', type: 'text', display: 'Expression threshold', value: null
        },
        direction: {
            name: 'direction',
            element_class: 'sample_annot',
            id: 'sample_annot_direction',
            type: 'dropdown',
            display: 'Label the samples > or < the threshold',
            values: [
                ['gt', 'above'],
                ['lt', 'below']
            ],
            selected: 'gt'
        }
    };

    if (cur_opts.automatic_group_name) {
        delete annot_fields.group;
        delete annot_fields.group_new;
    }

    group_opts = {
        // modify_user_content_url: modify_user_content_url,
        // auth_token: token,
        // series_pk: series_pk,
        // owner: owner,
        output_elem_id: '#sample_annot_output',
        annot_form_div: '#sample_annot_form',
        fields_schema: annot_fields
    };
    group_opts = combineObjects(group_opts, cur_opts);

    redrawFields(group_opts.fields_schema, group_opts.annot_form_div);
    if (! cur_opts.automatic_group_name) {
        refreshGroupNames(group_opts);
    }
    addFieldListeners(group_opts.fields_schema, group_opts.output_elem_id);

    $('#sample_annot_submit').click(function () {
        opts = {
            csrf_token: $('#sample_annot_csrf').children()[0].value,
            name: getInputValue('#sample_annot_name'),
            group: getInputValue('#sample_annot_group'),
            group_new: getInputValue('#sample_annot_group_new'),
            threshold: +getInputValue('#sample_annot_threshold'),
            direction: getInputValue('#sample_annot_direction')
        };
        opts = combineObjects(group_opts, opts);
        submitSampleAnnots(opts);

    });
}
