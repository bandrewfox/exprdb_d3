(function() {

// boxplot
// https://bl.ocks.org/mbostock/4061502
    d3.box = function() {
        var width = 1,
            height = 1,
            state = null,
            duration = 0,
            domain = null,
            value = Number,
            whiskers = boxWhiskers,
            quartiles = boxQuartiles,
            tickFormat = null;

        // For each set of points in a single group
        function box(g) {
            g.each(function(d, i) {
                d = d.map(value).sort(d3.ascending);
                var g = d3.select(this),
                    n = d.length;

                // Compute quartiles. Must return exactly 3 elements.
                var quartileData = d.quartiles = quartiles(d);

                // Compute whiskers. Must return exactly 2 elements, or null.
                var whiskerIndices = whiskers && whiskers.call(this, d, i),
                    whiskerData = whiskerIndices && whiskerIndices.map(function(i) { return d[i]; });

                // get the saved scales
                x1 = state.scales.xLinear;
                y1 = state.scales.yLinear;

                // Center line which covers most of the range: the vertical line spanning the whiskers.
                // only show if n >= 3
                if (n >= 3) {
                    g.selectAll("line.range")
                        .data(whiskerData ? [whiskerData] : [])
                        .enter()
                        .insert("line")
                        .attr("class", "range")
                        // .attr("x1", x1(d[0]))
                        // .attr("x2", x1(d[1]))
                        .attr("x1", function (d) {
                            return x1(d[0]);
                        })
                        .attr("x2", function (d) {
                            return x1(d[1]);
                        })
                        .attr("y1", y1(i + 0.50))
                        .attr("y2", y1(i + 0.50));
                }

                // IQR box
                // only show if n >= 4
                var boxWidth = 0.6;
                var boxOffset = (1-boxWidth)/2.0; // distance from edge of box to the adjacent data region
                if (n >= 4) {
                    g.selectAll("rect.box")
                        .data([quartileData])
                        .enter()
                        .insert("rect")
                        .attr("class", "box")
                        .attr("y", y1(i + boxOffset))
                        .attr("height", y1(boxWidth))
                        .attr("x", function (d) {
                            return x1(d[0]);
                        })
                        .attr("width", function (d) {
                            return x1(d[2]) - x1(d[0]);
                        });
                }

                // median line
                // always show the median
                g.selectAll("line.median")
                    .data([quartileData[1]])
                    .enter()
                    .insert("line")
                    .attr("class", "median")
                    .attr("y1", y1(i+boxOffset))
                    .attr("y2", y1(i+boxWidth+boxOffset))
                    .attr("x1", function(d) { return x1(d); })
                    .attr("x2", function(d) { return x1(d); });

                // 2 whisker lines (perpendicular lines at the end of the whiskers)
                // only show if n >= 3
                var whiskerWidth = boxWidth * 0.75;
                var whiskerOffset = (1-whiskerWidth)/2.0;
                if (n >= 3) {
                    g.selectAll("line.whisker")
                        .data(whiskerData || [])
                        .enter()
                        .insert("line")
                        .attr("class", "whisker")
                        .attr("y1", y1(i + whiskerOffset))
                        .attr("y2", y1(i + whiskerWidth + whiskerOffset))
                        .attr("x1", function (d) {
                            return x1(d);
                        })
                        .attr("x2", function (d) {
                            return x1(d);
                        });
                }

            });
            d3.timer.flush();
        }

        // All these functions are for initializing this boxchart class thing

        box.width = function(x) {
            if (!arguments.length) return width;
            width = x;
            return box;
        };

        box.height = function(x) {
            if (!arguments.length) return height;
            height = x;
            return box;
        };

        box.state = function(x) {
            if (!arguments.length) return state;
            state = x;
            return box;
        };

        box.tickFormat = function(x) {
            if (!arguments.length) return tickFormat;
            tickFormat = x;
            return box;
        };

        box.duration = function(x) {
            if (!arguments.length) return duration;
            duration = x;
            return box;
        };

        box.domain = function(x) {
            if (!arguments.length) return domain;
            domain = x === null ? x : d3.functor(x);
            return box;
        };

        box.value = function(x) {
            if (!arguments.length) return value;
            value = x;
            return box;
        };

        box.whiskers = function(x) {
            if (!arguments.length) return whiskers;
            whiskers = x;
            return box;
        };

        box.quartiles = function(x) {
            if (!arguments.length) return quartiles;
            quartiles = x;
            return box;
        };

        return box;
    };

    function boxWhiskers(d) {
        return [0, d.length - 1];
    }

    function boxQuartiles(d) {
        return [
            d3.quantile(d, .25),
            d3.quantile(d, .5),
            d3.quantile(d, .75)
        ];
    }

})();