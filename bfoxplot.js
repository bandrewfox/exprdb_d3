// bfoxplot.js

// jitter points
// https://bl.ocks.org/duhaime/raw/14c30df6b82d3f8094e5a51e5fff739a/

// boxplot
// https://bl.ocks.org/mbostock/4061502

(function () {
  // Returns a function to compute the interquartile range.
  function iqr(k) {
    return function (d, i) {
      var q1 = d.quartiles[0],
        q3 = d.quartiles[2],
        iqr = (q3 - q1) * k,
        i = -1,
        j = d.length;
      while (d[++i] < q1 - iqr);
      while (d[--j] > q3 + iqr);
      return [i, j];
    };
  }

  var state = {
    data: {
      nodes: null,
    },
    scales: {
      xLinear: null,
      yLinear: null,
      yOrdinal: null,
      colorLinear: null,
    },
    select2: {
      index: 0,
      organism: null,
      gene: null,
    },
    seriesInfo: [],
    expr_units: "",
    series_name: "",
    fields_schema: {
      series_pk: {
        name: "series_pk",
        id: "id_series_pk",
        type: "dropdown",
        display: "Series",
        values: null,
        selected: "",
      },
      gene: {
        name: "gene",
        id: "id_gene",
        type: "select2",
        display: "Gene",
        value: null,
      },
      groupby: {
        name: "groupby",
        id: "id_groupby",
        type: "dropdown",
        display: "Group By",
        values: null,
        selected: null,
      },
      colorby: {
        name: "colorby",
        id: "id_colorby",
        type: "dropdown",
        display: "Color By",
        values: null,
        selected: null,
      },
    },
  };

  var width = 600,
    height = 450;

  var boxchart = d3
    .box()
    .whiskers(iqr(1.5))
    .width(width)
    .height(height)
    .state(state);

  var margin = {
    top: 5,
    right: 10,
    bottom: 30,
    left: 120,
  };

  var chart = {
    container: "#bfoxplot",
    tooltip: "#bfoxplot-tooltip",
    label_tooltip: "#y-label-tooltip",
    width: width - margin.left - margin.right,
    height: height - margin.top - margin.bottom,
    levelHeight: 25,
    jitterVal: 4,
    pointSize: 4,
    data: {
      url_path: series_data_url,
      url_params: series_data_params,
    },
  };

  var hello = "hi";

  // make the svg element
  var svg = d3
    .select(chart.container)
    .append("svg")
    .attr("preserveAspectRatio", "xMidYMid meet")
    .attr("viewBox", "0 0 " + width + " " + height);

  // make the main graph area
  var g = svg
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  // add the grid
  var grid = g
    .append("rect")
    .attr("class", "grid")
    .attr("width", chart.width)
    .attr("height", chart.height);

  // add the x-axis
  g.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + chart.height + ")");

  // add the y-axis
  g.append("g").attr("class", "y axis").attr("transform", "translate(0,0)");

  // make a single object to hold all the boxplot boxeses
  g.append("g").attr("class", "allboxes");

  // Legend
  var svgLegend3 = d3.select(".legend3").append("svg").attr("height", height);
  // .attr('viewBox', '0 0 80 ' + height);
  // .attr("width", width)
  // .attr("height", height);

  // var neutral = g.append('line')
  //     .attr('class', 'neutral-line');

  // KEep the main data matrix as a "global"
  var dataMatrix;

  function updateChart() {
    var data = reshapeData(dataMatrix);
    var colorLabels = data.labels.clrs;
    var groupLabels = data.labels.grps;
    var nodes = data.observations;
    var boxgroups = data.boxgroups;

    chart.height = groupLabels.length * chart.levelHeight;
    height = chart.height + margin.top + margin.bottom;

    var valueDomain = d3.extent(
      nodes.map(function (d) {
        return d.value;
      })
    );

    var xScaleLinear = d3.scale
      .linear()
      .domain([valueDomain[0] - 0.015, valueDomain[1] + 0.015])
      .range([0, chart.width]);

    var yScaleLinear = d3.scale
      .linear()
      .domain([0, groupLabels.length])
      .range([0, chart.height]);

    var yScaleOrdinal = d3.scale
      .ordinal()
      .domain(groupLabels)
      .rangeBands([0, chart.height]);

    var rScale = d3.scale
      .linear()
      .range([3, 0.4])
      .domain([0, 1500])
      .clamp(true);

    var colorScaleLinear = d3.scale
      .category10()
      .domain([0, colorLabels.length]);

    var xAxis = d3.svg
      .axis()
      .scale(xScaleLinear)
      .innerTickSize(-chart.height)
      .tickPadding(10)
      .ticks(8)
      .orient("bottom");

    var yAxis = d3.svg
      .axis()
      .scale(yScaleOrdinal)
      .innerTickSize(-chart.width)
      .tickPadding(10)
      .orient("left")
      .tickFormat(function (d, i) {
        return truncateLabels(d);
      });

    /**
     * Cache scales, data, and domains for checkbox callbacks
     **/

    state.scales = {
      xLinear: xScaleLinear,
      yLinear: yScaleLinear,
      yOrdinal: yScaleOrdinal,
      colorLinear: colorScaleLinear,
      rScale: rScale,
    };

    state.data = {
      nodes: nodes,
      boxgroups: boxgroups,
    };

    /**
     * Transition elems
     **/

    svg.transition().attr("viewBox", "0 0 " + width + " " + height);

    grid.transition().attr("width", chart.width).attr("height", chart.height);

    g.select(".x.axis")
      .transition()
      .attr("transform", "translate(0," + chart.height + ")")
      .call(xAxis);

    yAxisLabels = g.select(".y.axis").transition().call(yAxis);

    g.select(".y.axis")
      .selectAll(".tick")[0]
      .forEach(function (d0, i) {
        d3.select(d0)
          .select("text")
          .on("mouseover", function () {
            var labelData = d3.select(d0).select("text").data();
            labelMousemove(labelData);
          })
          .on("mouseout", labelMouseout);
      });

    // axis labels
    d3.selectAll(".yAxisLabel").remove();
    g.select(".y.axis")
      .append("text")
      .attr("class", "yAxisLabel")
      .attr("text-anchor", "middle")
      .attr(
        "transform",
        "translate(" +
          (-1 * margin.left + 10) +
          "," +
          height / 2 +
          ")rotate(-90)"
      )
      .text(data.variables.grp);

    // d3.selectAll('.xAxisLabel').remove();
    // g.select('.x.axis')
    //     .append("text")
    //     .attr('class', 'xAxisLabel')
    //     .attr("text-anchor", "middle")
    //     .attr("transform", "translate("+ (margin.left + width/2) + "," + height + ")")
    //     .text(state.expr_units);

    /**
     * Make the boxplot part
     **/

    // start over by removing all DOM boxnodes
    g.selectAll(".boxnode").remove();

    // select all boxes
    var boxnode = g.select(".allboxes").selectAll(".boxnode").data(boxgroups);

    // don't need this since all boxes are removed each time, and this only removes the extra boxnodes
    // boxnode.exit()
    //     .remove();

    // make boxnodes and call the boxchart function on each one
    boxnode.enter().append("g").attr("class", "boxnode").call(boxchart);

    /**
     * Transition nodes
     **/
    // points
    numNodes = nodes.length;

    // select all data points
    var node = g.selectAll(".node").data(nodes);

    // remove the extra DOM nodes
    node.exit().remove();

    // add the extra DOM points (circles) one-by-one (via .enter)
    node
      .enter()
      .append("circle")
      .attr("class", "node")
      .style("opacity", 0)
      .on("mousemove", handleMousemove)
      .on("mouseout", handleMouseout);

    // draw all the points at the right locations and colors
    node
      .transition()
      .delay(function (d, i) {
        return (200 * i) / numNodes;
      })
      .duration(500)
      .attr("cx", function (d, i) {
        return state.scales.xLinear(d.value);
      })
      //.attr('cx', (d => state.scales.xLinear(d.value)))  // not supported on IE
      .attr("cy", getYPositionJitter)
      .attr("stroke", function (d, i) {
        return state.scales.colorLinear(d.clrInt);
      })
      .attr("fill", function (d, i) {
        return state.scales.colorLinear(d.clrInt);
      })
      .style("opacity", 0.75)
      .attr("r", state.scales.rScale(nodes.length));

    /**
     ** Legend
     **/

    // start by removing the legend
    d3.selectAll("g.legend3").remove();

    var labelHeight = 20;
    svgLegend3
      .transition()
      .duration(200)
      .attr("height", colorLabels.length * labelHeight);
    // .attr('viewBox', '0 0 80 ' + height);

    var legend3 = svgLegend3
      .selectAll(".legend3")
      .data(colorLabels)
      .enter()
      .append("g")
      .attr("class", "legend3")
      .attr("transform", function (d, i) {
        {
          return "translate(0," + i * labelHeight + ")";
        }
      });

    legend3
      .append("circle")
      .attr("cx", 5)
      .attr("cy", 5)
      .attr("r", 5)
      // .attr("height", 10)
      .style("fill", function (d, i) {
        return state.scales.colorLinear(i);
      });

    legend3
      .append("text")
      .attr("x", 20)
      .attr("y", 10)
      //.attr("dy", ".35em")
      .text(function (d, i) {
        return d;
      })
      .attr("class", "textselected")
      .style("text-anchor", "start")
      .style("font-size", 12);

    document.querySelector("#legend_title").innerHTML = data.variables.clr;
  }

  function getXPosition(d, i) {
    return state.scales.xLinear(d.value);
  }

  function getYPosition(d, i) {
    return (
      state.scales.yOrdinal(d.grpLabel) + state.scales.yOrdinal.rangeBand() / 2
    );
  }

  function getYPositionJitter(d, i) {
    var position = getYPosition(d, i);

    return i % 2 === 0
      ? position + Math.random() * chart.jitterVal
      : position - Math.random() * chart.jitterVal;
  }

  function getStroke(d) {
    return state.scales.colorLinear(d.clrInt);
  }

  function getFill(d) {
    return state.scales.colorLinear(d.clrInt);
  }

  /**
   * When the series_pk changes, then adjust the groupby and colorby dropdown choices
   **/

  /**
   * Checkbox callbacks
   **/

  // not used any more, but may add a button for this in the future, or a beeswarm option
  function handleJitter() {
    d3.selectAll(".node")
      .data(state.data.nodes)
      .transition()
      .delay(function (d, i) {
        return i * 3;
      })
      .attr("cy", getYPositionJitter);
  }

  /**
   * Tooltip handlers
   **/

  var tooltip = d3.select(chart.tooltip);
  var labelTooltip = d3.select(chart.label_tooltip);
  let boxtoolTipTimeout;
  let labelTooltipTimeout;

  function labelMousemove(data) {
    let mouseOverX = d3.event.layerX;
    let mouseOverY = d3.event.layerY;
    clearTimeout(labelTooltipTimeout);

    labelTooltipTimeout = setTimeout(function () {
      labelTooltip
        .style("left", mouseOverX + 20 + "px")
        .style("top", mouseOverY + 50 + "px")
        .style("opacity", 0.85)
        .style("z-index", 9);

      labelTooltip.select(".value").html(data);
    }, 1000);
  }

  function labelMouseout() {
    labelTooltip.style("opacity", 0).style("z-index", -1);
    clearTimeout(labelTooltipTimeout);
  }

  function handleMousemove(d, i) {
    let mouseOverX = d3.event.layerX;
    let mouseOverY = d3.event.layerY;
    let mouseOverSelection = this;
    clearTimeout(boxtoolTipTimeout);

    boxtoolTipTimeout = setTimeout(function () {
      //var mousePosition = d3.mouse(this);

      tooltip
        .style("left", mouseOverX + 20 + "px")
        .style("top", mouseOverY + 50 + "px")
        .style("opacity", 0.85)
        .style("z-index", 9);

      tooltip.select(".y-level").html(d.grpLabel);
      tooltip.select(".x-level").html(d.clrLabel);
      tooltip.select(".value").html(d.value);
      tooltip.select(".value").style("color", 0);

      d3.select(mouseOverSelection).style("opacity", 1);
    }, 1000);
  }

  function handleMouseout(d, i) {
    tooltip.style("opacity", 0).style("z-index", -1);
    clearTimeout(boxtoolTipTimeout);

    d3.select(this).style("opacity", 0.75);
  }

  /**
   * function to adjust the form choices with info from the specified series_id
   */
  function createSeriesInfo(results) {
    seriesInfo = [
      {
        choice_name: "choose dataset",
        id: "",
      },
    ];

    // read the json results for all the available series
    for (var i = 0; i < results.length; i++) {
      var curSeries = results[i];
      var curInfo = {
        id: curSeries.id.toString(),
        name: curSeries.name,
        organism: curSeries.organism,
        default_color: curSeries.details.default_color,
        default_group: curSeries.details.default_group,
        expr_units: curSeries.details.expr_units,
      };
      curInfo.choice_name = curSeries.name + " [" + curSeries.organism + "]";

      seriesInfo[seriesInfo.length] = curInfo;
    }
    return seriesInfo;
  }

  function readSampleAnnots() {
    if (!getSeries()) {
      document.querySelector("#plot_title_gene").innerHTML =
        "No series selected";
      document.querySelector("#plot_title_series_name").innerHTML = "";
      document.querySelector("#plot_expr_units").innerHTML = "";
    } else {
      d3.csv(getDatafilePath(), function (dat) {
        for (var i = 0; i < results; i++) {}
      });
    }
  }

  /**
   * Make the input fields if they don't already exist
   */

  function arrayContains(checkArray, v) {
    for (var i = 0; i < checkArray.length; i++) {
      if (checkArray[i] === v) return true;
    }
    return false;
  }

  function getUniqueValues(inputArray) {
    var temp_array = [];
    for (var i = 0; i < inputArray.length; i++) {
      if (!arrayContains(temp_array, inputArray[i])) {
        temp_array.push(inputArray[i]);
      }
    }
    return temp_array;
  }

  /** convert search/hash location url to json */
  // https://bl.ocks.org/cbertelegni/bb8fc7831219be8b39a1f8b32d813052
  function paramToObj(u) {
    var r = {};
    if (u) {
      u = decodeURIComponent(u.replace(/\?|\#/g, "")).split(/\&/);
      u.forEach(function (c, i) {
        c = c.split("=");

        var key = c[0].toLowerCase();
        var value = c[1];
        if (/^(null|false|true|[0-9]+)$/.test(value)) {
          value = JSON.parse(value);
        }

        if (key.match(/\[\]/g)) {
          key = key.replace(/\[\]/g, "");

          if (!r[key]) {
            r[key] = [];
          }
          r[key].push(value);
        } else {
          r[key] = value;
        }
      });
    }
    return r;
  }

  // // if the gene and series fields don't exist, then make all of the fields
  // if (null === document.querySelector('#id_gene') && null === document.querySelector('#id_series_pk')) {
  //     redrawFields();
  // }
  //

  /**
   * Bind data loaders to the controls
   **/

  var gene = null;
  var prevGene = null;
  var series_pk = null;
  var groupby = null;
  var colorby = null;

  function addListeners() {
    //gene = document.querySelector("#select2-id_gene-container");
    series_pk = document.querySelector("#id_series_pk");
    groupby = document.querySelector("#id_groupby");
    colorby = document.querySelector("#id_colorby");

    if (series_pk !== null) series_pk.addEventListener("change", changeSeries);
    //if (gene !== null) gene.addEventListener("change", reloadRedraw);
    $("#id_gene").on("select2:select", function (e) {
      gene = e.target.children.item(e.target.children.length - 1).textContent;
      state.select2.gene = gene;
      reloadRedraw();
    });
    if (groupby !== null) groupby.addEventListener("change", updateChart);
    if (colorby !== null) colorby.addEventListener("change", updateChart);
  }

  // jitter.addEventListener('change', handleJitter);

  function getGene() {
    if (gene === null) {
      return "";
    } else {
      return gene;
    }
  }

  function getPointSize() {
    return chart.pointSize;
  }

  function getSeries() {
    if (series_pk === null) {
      return null;
    } else {
      return series_pk.value;
    }
  }

  function getDatafilePath(series_pk, gene) {
    // 'http://127.0.0.1:8000/series_api/series_data_view/3?sample_labels=1&transpose=1&no_feat_info=1&name='
    return chart.data.url_path + series_pk + chart.data.url_params + gene;
  }

  // toUpper = function(x){ return x.toUpperCase()};

  function reshapeData(d) {
    // get all the values for the colors into an array and all the unique values into another array (labels)
    var myColors = d.map(function (item) {
      return item[colorby.value];
    });
    // var colorLabels = Array.from(new Set(myColors)).sort();
    var colorLabels = getUniqueValues(myColors).sort();
    var colorFactor = myColors.map(function (item) {
      return colorLabels.indexOf(item);
    });

    // same for groups
    var myGroups = d.map(function (item) {
      return item[groupby.value];
    });
    // var groupLabels = Array.from(new Set(myGroups)).sort();
    var groupLabels = getUniqueValues(myGroups).sort();
    var groupFactor = myGroups.map(function (item) {
      return groupLabels.indexOf(item);
    });

    // groupedObs is an Array of Arrays where elem 1 has the array of values from groupLabels == 1
    var groupedObs = new Array(groupLabels.length);
    for (var i = 0; i < groupLabels.length; i++) {
      groupedObs[i] = [];
    }

    // observations is an Array of objects with properties: color, grouping, value (x)
    var allObs = new Array(d.length);
    for (var i = 0; i < d.length; i++) {
      allObs[i] = {
        value: d[i][gene], //[gene.value]
        grpInt: groupFactor[i],
        clrInt: colorFactor[i],
        clrLabel: colorLabels[colorFactor[i]],
        grpLabel: groupLabels[groupFactor[i]],
      };
      groupedObs[groupFactor[i]].push(d[i][gene]); //[gene.value]
    }

    return {
      labels: { grps: groupLabels, clrs: colorLabels },
      observations: allObs,
      boxgroups: groupedObs,
      variables: { grp: groupby.value, clr: colorby.value },
    };
  }

  function changeSeries() {
    if (getSeries()) {
      var prevOrganism = state.select2.organism;
      d3.csv(getDatafilePath(getSeries(), ""), function (dat) {
        if (dat) {
          annot_choices = [];
          Object.keys(dat[0]).forEach(function (ann) {
            if (ann !== "_id") annot_choices[annot_choices.length] = ann;
          });

          // get the default group and color for the selected series
          default_group = default_color = expr_units = null;
          for (var i = 0; i < state.seriesInfo.length; i++) {
            if (state.seriesInfo[i].id === getSeries()) {
              default_group = state.seriesInfo[i].default_group;
              default_color = state.seriesInfo[i].default_color;
              expr_units = state.seriesInfo[i].expr_units;
              series_name = state.seriesInfo[i].name;
              state.select2.organism = state.seriesInfo[i].organism;
            }
          }
          if (prevOrganism != state.select2.organism && getGene()) {
            var apiCall =
              "http://demo.needlegenomics.com/feature_api/feature/?search_type=begins&organism=" +
              state.select2.organism +
              "&name=" +
              getGene() +
              "&search_type=exact";
            d3.json(apiCall, function (data) {
              if (data.count > 0) {
                gene = data.results[0].name;
                state.select2.gene = gene;
              }
            });
          }
          if (!default_color) default_color = default_group;

          state.fields_schema.groupby.values = annot_choices;
          state.fields_schema.groupby.selected = default_group;

          state.fields_schema.colorby.values = annot_choices;
          state.fields_schema.colorby.selected = default_color;

          state.fields_schema.series_pk.selected = getSeries();
          state.fields_schema.gene.value = getGene();

          state.expr_units = expr_units;
          state.series_name = series_name;

          redrawFields(state.fields_schema, "#inputs", state.select2);
          addListeners();
          if (getGene() && getSeries()) {
            reloadRedraw();
          }
        }
      });
    }
  }

  /**
   * Main function that triggers chart updates
   **/
  function reloadRedraw() {
    if (!getSeries()) {
      // If there is no series_pk (or it is 0 or blank), then load the series info
      // this is only done once! (when the page first loads)
      d3.json(all_series_names_url, function (dat) {
        res = dat.results;
        state.seriesInfo = createSeriesInfo(res);

        // save the choices for the series_pk dropdown
        state.fields_schema.series_pk.values = state.seriesInfo.map(function (
          d
        ) {
          return [d.id, d.choice_name];
        });

        // check if a gene or series_pk is specified as a GET param
        // (save to state.fields_schema)
        curParams = paramToObj(location.search);
        if (curParams["series_pk"]) {
          state.fields_schema.series_pk.selected = curParams.series_pk.toString();
        }
        if (curParams["gene"]) {
          state.fields_schema.gene.value = curParams.gene.toString();
        }
        // add the field <div> and elements
        redrawFields(
          //subObject(state.fields_schema, ["series_pk", "gene"]),
          subObject(state.fields_schema, ["series_pk"]),
          "#inputs",
          state.select2
        );
        addListeners();

        if (state.fields_schema.series_pk.selected) {
          // this will load all the annotations to the groupby and colorby dropdowns
          // for the current series_pk (which would be from the URL)
          changeSeries();
        }
      });
    }

    if (getSeries() && getGene()) {
      // document.querySelector('#plot_gene_url').href = plot_gene_url + '?gene=' + getGene() + '&series_pk=' + getSeries();
      d3.csv(getDatafilePath(getSeries(), getGene()), function (dat) {
        if (dat.length > 0 && dat[0][getGene()]) {
          // change the values in the gene expression column to numeric
          dat.forEach(function (d) {
            d[getGene()] = +d[getGene()];
          });
          dataMatrix = dat;
          updateChart();
          document.querySelector("#plot_title_gene").innerHTML = getGene();
          document.querySelector("#plot_title_series_name").innerHTML =
            state.series_name;
          if (expr_units) {
            document.querySelector("#plot_expr_units").innerHTML =
              state.expr_units;
          } else {
            document.querySelector("#plot_expr_units").innerHTML = "Expression";
          }
          state.fields_schema.gene.value = getGene();
        } else {
          document.querySelector("#plot_title_gene").innerHTML =
            "Gene not found: " + getGene();
          document.querySelector("#plot_title_series_name").innerHTML = "";
          document.querySelector("#plot_expr_units").innerHTML = "";
        }
      });
    }
  }

  function truncateLabels(label) {
    if (label.length <= 12) {
      return label;
    } else {
      return label.substr(0, 9) + "...";
    }
  }

  addListeners();

  // initialize the chart
  reloadRedraw();
})();
